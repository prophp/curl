<?php

/**
* This file was automatically generated with prophp/render-params package
*/

namespace ProPhp\Curl;

use ProPhp\Params;

class JsonParams extends Params
{
    protected bool $jsonDecodeResponse = true;

    public function jsonDecodeResponse(bool $jsonDecodeResponse = null): bool|self
    {
        return $this->getOrSet(get_defined_vars());
    }

    protected bool $responseToArray = true;

    public function responseToArray(bool $responseToArray = null): bool|self
    {
        return $this->getOrSet(get_defined_vars());
    }

    protected bool $jsonEncodeRequestPostData = true;

    public function jsonEncodeRequestPostData(bool $jsonEncodeRequestPostData = null): bool|self
    {
        return $this->getOrSet(get_defined_vars());
    }


}