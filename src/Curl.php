<?php

namespace ProPhp\Curl;

class Curl
{
    private static function prepareUrl(string $url, CurlParams $params): string
    {
        if (!empty($params->urlQueryData())) {
            if (str_contains($url, '?')) {
                if (!str_ends_with($url, '?')) {
                    $url .= "&";
                }
            } else {
                $url .= "?";
            }

            $url .= http_build_query($params->urlQueryData());
        }

        return $url;
    }

    public static function sendRequest(string $url, ?CurlParams $params = null): string
    {
        $params = $params ?? new CurlParams();

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, self::prepareUrl($url, $params));
        curl_setopt($curl, CURLOPT_USERAGENT, $params->agent());
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, $params->returnTransfer());
        curl_setopt($curl, CURLOPT_VERBOSE, $params->verbose());
        curl_setopt($curl, CURLOPT_TIMEOUT, $params->timeout());
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, $params->followLocation());


        if (!empty($params->postRequestData())) {
            curl_setopt($curl, CURLOPT_POST, 1);
            // @todo Only in sendJsonRequest()
            curl_setopt($curl, CURLOPT_POSTFIELDS, $params->postRequestData());

            if (in_array($params->method(), ['PUT', 'DELETE'], true)) {
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $params->method());
            }
        } elseif ($params->method() !== 'GET') {
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $params->method());
        }


        curl_setopt($curl, CURLOPT_HTTPHEADER, $params->headers());

        $output = curl_exec($curl);

        curl_close($curl);

        if (!$output && curl_errno($curl)) {
            throw new \Exception(curl_error($curl), curl_errno($curl));
        }

        return $output;
    }

    public static function json(string $url, ?CurlParams $params = null, ?JsonParams $jsonParams = null): mixed
    {
        $params = $params ?? new CurlParams();
        $jsonParams = $jsonParams ?? new JsonParams();

        $params->headers(array_merge(["Content-Type: application/json"], $params->headers()));

        if ($jsonParams->jsonEncodeRequestPostData() && !empty($params->postRequestData())) {
            $params->postRequestData(json_encode($params->postRequestData()));
        }

        $response = self::sendRequest($url, $params);

        if ($jsonParams->jsonDecodeResponse()) {
            $toArray = $jsonParams->responseToArray();
            return json_decode($response, $toArray);
        }

        return $response;
    }

    // @todo Develop later
    public static function sendXmlRequest(string $url, $postData, array $additionalHeaders = []): string
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:17.0) Gecko/20100101 Firefox/17.0');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POST, 1);

        $headers = ["Content-Type: text/xml; charset=utf-8"];
        $headers[] = "Content-Length: " . strlen($postData);
        $headers = array_merge($headers, $additionalHeaders);
        curl_setopt($curl, CURLOPT_VERBOSE, false);

        curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        $output = curl_exec($curl);

        curl_close($curl);

        return $output;
    }
}