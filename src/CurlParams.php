<?php

/**
* This file was automatically generated with prophp/render-params package
*/

namespace ProPhp\Curl;

use ProPhp\Params;

class CurlParams extends Params
{
    protected string $agent = 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:17.0) Gecko/20100101 Firefox/17.0';

    public function agent(string $agent = null): string|self
    {
        return $this->getOrSet(get_defined_vars());
    }

    protected array $headers = [];

    public function headers(array $headers = null): array|self
    {
        return $this->getOrSet(get_defined_vars());
    }

    protected bool $returnTransfer = true;

    public function returnTransfer(bool $returnTransfer = null): bool|self
    {
        return $this->getOrSet(get_defined_vars());
    }

    protected array $urlQueryData = [];

    public function urlQueryData(array $urlQueryData = null): array|self
    {
        return $this->getOrSet(get_defined_vars());
    }

    protected string|array $postRequestData = '';

    public function postRequestData(string|array $postRequestData = null): string|array|self
    {
        return $this->getOrSet(get_defined_vars());
    }

    protected bool $verbose = false;

    public function verbose(bool $verbose = null): bool|self
    {
        return $this->getOrSet(get_defined_vars());
    }

    protected int $timeout = 0;

    public function timeout(int $timeout = null): int|self
    {
        return $this->getOrSet(get_defined_vars());
    }

    protected string $method = 'GET';

    public function method(string $method = null): string|self
    {
        return $this->getOrSet(get_defined_vars());
    }

    protected bool $followLocation = true;

    public function followLocation(bool $followLocation = null): bool|self
    {
        return $this->getOrSet(get_defined_vars());
    }


}